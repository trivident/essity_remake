﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace myDD4Tproject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");




            routes.MapRoute(
                name: "Home",
                url: "",
                //defaults: new { controller = "Page", action = "FindPage" }
                defaults: new { controller = "Page", action = "HomePage" }
            );


            routes.MapRoute(
                name: "Releases",
                url: "media/press-releases/",
                //defaults: new { controller = "Page", action = "FindPage" }
                defaults: new { controller = "Media", action = "GetAllPressReleases" }
            );

            routes.MapRoute(
                name: "Press",
                url: "media/press-release/{id}",
                //defaults: new { controller = "Page", action = "FindPage" }
                defaults: new { controller = "Media", action = "FindPressRelease" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Page", action = "FindPage", id = UrlParameter.Optional }
            );
        }
    }
}
