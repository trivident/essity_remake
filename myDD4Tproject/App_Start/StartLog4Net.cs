using log4net.Config;

[assembly: WebActivatorEx.PreApplicationStartMethod(
typeof(myDD4Tproject.App_Start.StartLog4Net), "Start")]

namespace myDD4Tproject.App_Start
{
    public static class StartLog4Net
    {
        public static void Start()
        {
            XmlConfigurator.Configure();
        }
    }
}