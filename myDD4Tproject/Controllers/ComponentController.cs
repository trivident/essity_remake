﻿using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.Controllers;
using myDD4Tproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myDD4Tproject.Controllers
{
    public class ComponentController : ModelControllerBase
    {
        private IPageFactory _pageFactory;
        private IComponentPresentationFactory _componentPresentationFactory;
        private ILogger _logger;
        private IDD4TConfiguration _dd4tConfiguration;
        private IViewModelFactory _viewModelFactory;
        public ComponentController(IPageFactory pageFactory, 
            IComponentPresentationFactory componentPresentationFactory, 
            ILogger logger, 
            IDD4TConfiguration dd4tConfiguration, 
            IViewModelFactory viewModelFactory) : base(pageFactory, componentPresentationFactory, logger, dd4tConfiguration, viewModelFactory)
        {
            _pageFactory = pageFactory;
            _componentPresentationFactory = componentPresentationFactory;
            _logger = logger;
            _dd4tConfiguration = dd4tConfiguration;
            _viewModelFactory = viewModelFactory;
        }

        // GET: Component
        public ActionResult Index()
        {
            return View();
        }

    }
}