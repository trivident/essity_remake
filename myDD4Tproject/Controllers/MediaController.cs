﻿using DD4T.ContentModel;
using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using myDD4Tproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tridion.ContentDelivery.DynamicContent.Query;

namespace myDD4Tproject.Controllers
{
    public class MediaController : Controller
    {
        private IPageFactory _pageFactory;
        private IComponentPresentationFactory _componentPresentationFactory;
        private IViewModelFactory _viewModelFactory;
        private ILogger _logger;
        private IDD4TConfiguration _dd4tConfiguration;
        public MediaController(IPageFactory pageFactory,
            IComponentPresentationFactory componentPresentationFactory,
            ILogger logger,
            IDD4TConfiguration dd4tConfiguration,
            IViewModelFactory viewModelFactory)
        {
            _pageFactory = pageFactory;
            _componentPresentationFactory = componentPresentationFactory;
            _logger = logger;
            _dd4tConfiguration = dd4tConfiguration;
            _viewModelFactory = viewModelFactory;
        }

        // Find a pressrelease
        public ActionResult GetAllPressReleases(string category)
        {
            ViewBag.Title = "Press releases";

            Criteria schemaCriteria = new SchemaTitleCriteria("PressRelease");
            Query query = new Query(schemaCriteria);

            if (category != null && category == "reports")
            {
                query.Criteria = new OrCriteria(new CustomMetaValueCriteria("Interim Report"), new CustomMetaValueCriteria("Annual Report"));
            }
            CustomMetaKeyColumn customMetaKeyColumn = new CustomMetaKeyColumn("Date", MetadataType.DATE);
            SortParameter sortParameter = new SortParameter(customMetaKeyColumn, SortParameter.Descending);
            query.AddSorting(sortParameter);
            var results = query.ExecuteQuery();

            var cps = _componentPresentationFactory.GetComponentPresentations(results);
            List<PressRelease> pressReleases = new List<PressRelease>();
            foreach (var cp in cps)
            {
                PressRelease pressRelease = _viewModelFactory.BuildViewModel<PressRelease>(cp.Component);

                if(pressRelease.Introduction != null && pressRelease.Introduction.Count(f=> f == ' ') >= 30)
                {
                    var words = pressRelease.Introduction.Split(' ').Take(30);
                    pressRelease.Introduction = String.Join(" ", words) + "[...]";
                }
                pressReleases.Add(pressRelease);
            }
            return View("allPressReleases", pressReleases);
        }


        // Find a press release

        public ActionResult FindPressRelease(string id)
        {
            Query query = new Query();
            query.Criteria = new CustomMetaValueCriteria(id);
            var results = query.ExecuteQuery();
            IComponentPresentation cp;
            if(results.Length != null && results.Length == 1 && _componentPresentationFactory.TryGetComponentPresentation(out cp,results[0]))
            {
                PressRelease pressRelease = (PressRelease)_viewModelFactory.BuildViewModel<PressRelease>(cp.Component);
                ViewBag.Title = pressRelease.Title;
                return View("pressRelease", pressRelease);
            }
            return GetAllPressReleases("");
        }

        public ActionResult GetLatestPressReleases()
        {
            QueryWidget viewModel = RouteData.Values["model"] as QueryWidget;
            
            Criteria schemaCriteria = new SchemaTitleCriteria("PressRelease");
            Query query = new Query(schemaCriteria);

            CustomMetaKeyColumn customMetaKeyColumn = new CustomMetaKeyColumn("Date", MetadataType.DATE);
            SortParameter sortParameter = new SortParameter(customMetaKeyColumn, SortParameter.Descending);
            query.AddSorting(sortParameter);
            query.SetResultFilter(new LimitFilter(3));
            var results = query.ExecuteQuery();

            var cps = _componentPresentationFactory.GetComponentPresentations(results);
            viewModel.PressReleases = new List<PressRelease>();
            foreach (var cp in cps)
            {
                viewModel.PressReleases.Add(_viewModelFactory.BuildViewModel<PressRelease>(cp.Component));
            }

            string view = RouteData.Values["view"] as string;

            return PartialView(view, viewModel);

        }
        public ActionResult GetLatestFinancialReports()
        {
            QueryWidget viewModel = RouteData.Values["model"] as QueryWidget;
            
            Criteria schemaCriteria = new SchemaTitleCriteria("PressRelease");
            Query query = new Query(schemaCriteria);

            CustomMetaKeyColumn customMetaKeyColumn = new CustomMetaKeyColumn("Date", MetadataType.DATE);
            SortParameter sortParameter = new SortParameter(customMetaKeyColumn, SortParameter.Descending);
            
            query.Criteria = new OrCriteria(new CustomMetaValueCriteria("Interim Report"), new CustomMetaValueCriteria("Annual Report"));
            query.AddSorting(sortParameter);
            query.SetResultFilter(new LimitFilter(3));
            var results = query.ExecuteQuery();

            var cps = _componentPresentationFactory.GetComponentPresentations(results);
            viewModel.PressReleases = new List<PressRelease>();
            foreach (var cp in cps)
            {
                viewModel.PressReleases.Add(_viewModelFactory.BuildViewModel<PressRelease>(cp.Component));
            }

            string view = RouteData.Values["view"] as string;

            return PartialView(view, viewModel);

        }

        public ActionResult CallToAction()
        {
            CallToAction viewModel = RouteData.Values["model"] as CallToAction;

            string view = RouteData.Values["view"] as string;
            return PartialView(view, viewModel);
        }


    }
}