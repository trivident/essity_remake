﻿using DD4T.ContentModel;
using DD4T.ContentModel.Contracts.Configuration;
using DD4T.ContentModel.Contracts.Logging;
using DD4T.ContentModel.Factories;
using DD4T.Core.Contracts.ViewModels;
using myDD4Tproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tridion.ContentDelivery.DynamicContent.Query;

namespace myDD4Tproject.Controllers
{
    public class PageController : Controller
    {
        private IPageFactory _pageFactory;
        private IComponentPresentationFactory _componentPresentationFactory;
        private IViewModelFactory _viewModelFactory;
        private ILogger _logger;
        private IDD4TConfiguration _dd4tConfiguration;
        public PageController(IPageFactory pageFactory,
            IComponentPresentationFactory componentPresentationFactory,
            ILogger logger,
            IDD4TConfiguration dd4tConfiguration,
            IViewModelFactory viewModelFactory)
        {
            _pageFactory = pageFactory;
            _componentPresentationFactory = componentPresentationFactory;
            _logger = logger;
            _dd4tConfiguration = dd4tConfiguration;
            _viewModelFactory = viewModelFactory;
        }

        // Find a pressrelease
        public ActionResult HomePage()
        {          
            ViewBag.Title = "Welcome to Essity - A Leading Hygiene and Health Company";

            var page = _pageFactory.FindPage("/index.html");
            //var queryModel = _viewModelFactory.BuildViewModel(page.ComponentPresentations.FirstOrDefault());
            var pageModel = _viewModelFactory.BuildViewModel<StandardPageModel>(page);


            /* Getting latest press releases
            Criteria schemaCriteria = new SchemaTitleCriteria("PressRelease");
            Query query = new Query(schemaCriteria);
            
            CustomMetaKeyColumn customMetaKeyColumn = new CustomMetaKeyColumn("Date", MetadataType.DATE);
            SortParameter sortParameter = new SortParameter(customMetaKeyColumn, SortParameter.Descending);
            query.AddSorting(sortParameter);
            query.SetResultFilter(new LimitFilter(3));
            var results = query.ExecuteQuery();
            
            var cps = _componentPresentationFactory.GetComponentPresentations(results);
            List<PressRelease> pressReleases = new List<PressRelease>();
            foreach (var cp in cps)
            {
                pressReleases.Add(_viewModelFactory.BuildViewModel<PressRelease>(cp.Component));
            }
            
            // Getting latest reports
            query = new Query(schemaCriteria);
            
            query.Criteria = new OrCriteria(new CustomMetaValueCriteria("Interim Report"), new CustomMetaValueCriteria("Annual Report"));
            query.AddSorting(sortParameter);
            query.SetResultFilter(new LimitFilter(3));
            results = query.ExecuteQuery();
            
            cps = _componentPresentationFactory.GetComponentPresentations(results);
            List<PressRelease> reports = new List<PressRelease>();
            foreach (var cp in cps)
            {
                reports.Add(_viewModelFactory.BuildViewModel<PressRelease>(cp.Component));
            }
            
            HomePage hp = new HomePage(pressReleases, reports);
            hp.AllItems = page.AllItems;*/
            string smth = "";
            foreach (var item in pageModel.AllItems)
            {
                smth += item.GetType().ToString() + '\n';
            }

            return View("index", pageModel);
        }

        
    }
}