﻿
using DD4T.DI.Autofac;
using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using myDD4Tproject.App_Start;
using System.Web.Optimization;

namespace myDD4Tproject
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterFilterProvider();
            builder.UseDD4T();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
    }
}
