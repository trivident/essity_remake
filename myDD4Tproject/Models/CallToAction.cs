﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myDD4Tproject.Models
{

    [ContentModel("callToAction", true)]
    public class CallToAction : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Title { get; set; }

        [TextField]
        public string Text { get; set; }

        [TextField]
        public string Image { get; set; }

        [TextField(IsMetadata = true)]
        public string Address { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}