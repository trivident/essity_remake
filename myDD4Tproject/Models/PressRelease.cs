﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myDD4Tproject.Models
{
    [ContentModel("pressRelease", true)]
    public class PressRelease : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Title { get; set; }


        [TextField]
        public string Introduction { get; set; }

        [RichTextField]
        public MvcHtmlString Body { get; set; }
        
        [TextField]
        public string File { get; set; }

        [DateField(IsMetadata = true)]
        public DateTime Date { get; set; }

        [TextField(IsMetadata = true)]
        public string Address { get; set; }

        [TextField(IsMetadata = true)]
        public string Category { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}