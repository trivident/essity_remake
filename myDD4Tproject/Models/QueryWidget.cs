﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace myDD4Tproject.Models
{
    [ContentModel("queryWidget", true)]
    public class QueryWidget : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Heading { get; set; }


        [NumberField]
        public double NrOfItems { get; set; }

        
        [TextField]
        public string ReadAllLinkText { get; set; }


    
        public List<PressRelease> PressReleases { get; set; }


        [RenderData]
        public IRenderData RenderData { get; set; }
       
    }
}