﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myDD4Tproject.Models
{
    [PageViewModel(TemplateTitle = "Standard")]
    public class StandardPageModel : ViewModelBase
    {
        [PageTitle]
        public string PageTitle { get; set; }

        [TextField(IsMetadata = true)]
        public List<string> Keywords { get; set; }

        [ComponentPresentations]
        public List<IRenderableViewModel> AllItems
        {
            get; set;
        }

        public StandardPageModel() { }
    }
}